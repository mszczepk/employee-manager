package mszczepk.employeemanager.controller;

import mszczepk.employeemanager.domain.dto.ExceptionData;
import mszczepk.employeemanager.domain.error.StatusException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {
    @ExceptionHandler
    public ResponseEntity<ExceptionData> handle(StatusException exception) {
        HttpHeaders headers = new HttpHeaders();
        ExceptionData body = exception.toExceptionData();
        HttpStatus status = exception.getStatus();
        return new ResponseEntity<>(body, headers, status);
    }
}
