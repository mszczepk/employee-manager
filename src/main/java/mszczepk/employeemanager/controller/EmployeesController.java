package mszczepk.employeemanager.controller;

import mszczepk.employeemanager.domain.dto.EmployeeCreationData;
import mszczepk.employeemanager.domain.dto.EmployeeInfo;
import mszczepk.employeemanager.domain.error.BadRequestException;
import mszczepk.employeemanager.domain.error.NotFoundException;
import mszczepk.employeemanager.domain.error.StatusException;
import mszczepk.employeemanager.service.EmployeeCreationService;
import mszczepk.employeemanager.service.EmployeeDeletionService;
import mszczepk.employeemanager.service.EmployeeSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Map;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/employees")
public class EmployeesController {
    private EmployeeCreationService employeeCreationService;
    private EmployeeDeletionService employeeDeletionService;
    private EmployeeSearchService employeeSearchService;

    @Autowired
    public EmployeesController(EmployeeCreationService employeeCreationService,
                               EmployeeDeletionService employeeDeletionService,
                               EmployeeSearchService employeeSearchService) {
        this.employeeCreationService = employeeCreationService;
        this.employeeDeletionService = employeeDeletionService;
        this.employeeSearchService = employeeSearchService;
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(code = CREATED)
    public EmployeeInfo post(@RequestBody EmployeeCreationData employeeRequest) throws StatusException {
        return employeeCreationService.createEmployee(employeeRequest);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<EmployeeInfo> get(@RequestParam Map<String, String> parameters) throws BadRequestException {
        if (parameters.isEmpty()) {
            return employeeSearchService.getAll();
        } else {
            return employeeSearchService.getAllBySpecification(parameters);
        }
    }

    @DeleteMapping(path = "/{id}")
    public EmployeeInfo delete(@PathVariable("id") Long id) throws NotFoundException {
        return employeeDeletionService.deleteEmployee(id);
    }
}
