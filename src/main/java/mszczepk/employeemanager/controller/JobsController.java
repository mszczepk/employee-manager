package mszczepk.employeemanager.controller;

import mszczepk.employeemanager.domain.dto.JobCreationData;
import mszczepk.employeemanager.domain.dto.JobStatistic;
import mszczepk.employeemanager.domain.error.StatusException;
import mszczepk.employeemanager.service.JobCreationService;
import mszczepk.employeemanager.service.JobStatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/jobs")
public class JobsController {
    private JobCreationService jobCreationService;
    private JobStatisticService jobStatisticService;

    @Autowired
    public JobsController(JobCreationService jobCreationService, JobStatisticService jobStatisticService) {
        this.jobCreationService = jobCreationService;
        this.jobStatisticService = jobStatisticService;
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(code = CREATED)
    public JobStatistic post(@RequestBody JobCreationData jobRequest) throws StatusException {
        return jobCreationService.createJob(jobRequest);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<JobStatistic> getAll() {
        return jobStatisticService.getAllJobStatistics();
    }
}
