package mszczepk.employeemanager.domain.dto;

import lombok.Data;

@Data
public class JobCreationData {
    private String title;
}
