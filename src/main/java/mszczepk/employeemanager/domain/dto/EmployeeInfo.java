package mszczepk.employeemanager.domain.dto;

import lombok.Data;
import mszczepk.employeemanager.domain.entity.Employee;

@Data
public class EmployeeInfo {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String job;

    public EmployeeInfo(Employee employee) {
        id = employee.getId();
        firstName = employee.getFirstName();
        lastName = employee.getLastName();
        email = employee.getEmail();
        job = employee.getJob().getTitle();
    }
}
