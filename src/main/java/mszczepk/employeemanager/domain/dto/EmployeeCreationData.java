package mszczepk.employeemanager.domain.dto;

import lombok.Data;

@Data
public class EmployeeCreationData {
    private String firstName;
    private String lastName;
    private String email;
    private String job;
}
