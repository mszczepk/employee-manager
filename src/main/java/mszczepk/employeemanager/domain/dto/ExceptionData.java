package mszczepk.employeemanager.domain.dto;

import lombok.Data;
import mszczepk.employeemanager.domain.error.StatusException;
import java.util.Date;

@Data
public class ExceptionData {
    private int code;
    private String status;
    private String exception;
    private String message;
    private Date timestamp;

    public ExceptionData(StatusException e) {
        code = e.getStatus().value();
        status = e.getStatus().getReasonPhrase();
        exception = e.getClass().getSimpleName();
        message = e.getMessage();
        timestamp = new Date();
    }
}
