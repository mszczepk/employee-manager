package mszczepk.employeemanager.domain.dto;

import lombok.Getter;
import lombok.Setter;
import mszczepk.employeemanager.domain.error.StatusException;
import java.util.List;

public class BadRequestExceptionData extends ExceptionData {
    @Getter @Setter
    private List<String> problems;

    public BadRequestExceptionData(StatusException e, List<String> problems) {
        super(e);
        this.problems = problems;
    }
}
