package mszczepk.employeemanager.domain.dto;

import lombok.Data;
import mszczepk.employeemanager.domain.entity.Job;

@Data
public class JobStatistic {
    private Long id;
    private String title;
    private Long employeesCount;

    public JobStatistic(Job job) {
        id = job.getId();
        title = job.getTitle();
        employeesCount = 0L;
    }

    public JobStatistic(Job job, Long employeesCount) {
        this(job);
        this.employeesCount = employeesCount;
    }
}
