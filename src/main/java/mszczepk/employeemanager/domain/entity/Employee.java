package mszczepk.employeemanager.domain.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;
import static javax.persistence.FetchType.LAZY;

@Data
@Entity
public class Employee {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false, unique = true)
    private String email;

    @ManyToOne(optional = false, fetch = LAZY)
    private Job job;

    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    private Date createdOn;
}
