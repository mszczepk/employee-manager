package mszczepk.employeemanager.domain.error;

import mszczepk.employeemanager.domain.dto.BadRequestExceptionData;
import mszczepk.employeemanager.domain.dto.ExceptionData;
import java.util.List;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

public class BadRequestException extends StatusException {
    private List<String> problems;

    public BadRequestException() {
        super(BAD_REQUEST, "Invalid data.");
    }

    public BadRequestException(String message) {
        super(BAD_REQUEST, message);
        problems = null;
    }

    public BadRequestException(List<String> problems) {
        super(BAD_REQUEST, "Invalid data in request body.");
        this.problems = problems;
    }

    @Override
    public ExceptionData toExceptionData() {
        if (problems == null) {
            return new ExceptionData(this);
        } else {
            return new BadRequestExceptionData(this, problems);
        }
    }
}
