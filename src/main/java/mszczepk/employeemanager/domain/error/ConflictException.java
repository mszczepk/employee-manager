package mszczepk.employeemanager.domain.error;

import static org.springframework.http.HttpStatus.CONFLICT;

public class ConflictException extends StatusException {
    public ConflictException() {
        super(CONFLICT, "Resource already exists.");
    }

    public ConflictException(String message) {
        super(CONFLICT, message);
    }
}
