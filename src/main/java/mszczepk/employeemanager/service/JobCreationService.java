package mszczepk.employeemanager.service;

import mszczepk.employeemanager.domain.dto.JobCreationData;
import mszczepk.employeemanager.domain.dto.JobStatistic;
import mszczepk.employeemanager.domain.error.StatusException;

public interface JobCreationService {
    JobStatistic createJob(JobCreationData jobRequest) throws StatusException;
}
