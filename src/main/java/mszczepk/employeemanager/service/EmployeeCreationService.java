package mszczepk.employeemanager.service;

import mszczepk.employeemanager.domain.dto.EmployeeCreationData;
import mszczepk.employeemanager.domain.dto.EmployeeInfo;
import mszczepk.employeemanager.domain.error.StatusException;

public interface EmployeeCreationService {
    EmployeeInfo createEmployee(EmployeeCreationData employeeRequest) throws StatusException;
}
