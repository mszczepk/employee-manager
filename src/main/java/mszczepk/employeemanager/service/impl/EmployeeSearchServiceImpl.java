package mszczepk.employeemanager.service.impl;

import mszczepk.employeemanager.domain.dto.EmployeeInfo;
import mszczepk.employeemanager.domain.error.BadRequestException;
import mszczepk.employeemanager.domain.entity.Employee;
import mszczepk.employeemanager.persistence.query.EmployeeSpecification;
import mszczepk.employeemanager.persistence.repository.EmployeeRepository;
import mszczepk.employeemanager.service.EmployeeSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class EmployeeSearchServiceImpl implements EmployeeSearchService {
    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeSearchServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<EmployeeInfo> getAll() {
        Stream<Employee> employees = employeeRepository.findAll().stream();
        return toEmployeeInfoList(employees);
    }

    private List<EmployeeInfo> toEmployeeInfoList(Stream<Employee> employees) {
        Stream<EmployeeInfo> employeeInfos = employees.map(EmployeeInfo::new);
        return employeeInfos.collect(Collectors.toList());
    }

    @Override
    public List<EmployeeInfo> getAllBySpecification(Map<String, String> parameters) throws BadRequestException {
        validate(parameters);
        Specification<Employee> specification = conjunctSpecificationFrom(parameters);
        Stream<Employee> employees = employeeRepository.findAll(specification).stream();
        Stream<EmployeeInfo> employeeInfos = employees.map(EmployeeInfo::new);
        return employeeInfos.collect(Collectors.toList());
    }

    private void validate(Map<String, String> parameters) throws BadRequestException {
        List<String> validFieldNames = Arrays.asList("firstName", "lastName", "email");
        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            String fieldName = entry.getKey();
            if (!validFieldNames.contains(fieldName)) {
                throw new BadRequestException("Invalid parameter: '" + fieldName +"'.");
            }
        }
    }

    private Specification<Employee> conjunctSpecificationFrom(Map<String, String> parameters) {
        List<Specification<Employee>> specifications = createSpecifications(parameters);
        return conjunct(specifications);
    }

    private List<Specification<Employee>> createSpecifications(Map<String, String> parameters) {
        Stream<Map.Entry<String, String>> entries = parameters.entrySet().stream();
        Stream<EmployeeSpecification> specifications = entries.map(entry -> {
            String fieldName = entry.getKey();
            String fieldValue = entry.getValue();
            return new EmployeeSpecification(fieldName, fieldValue);
        });
        return specifications.collect(Collectors.toList());
    }

    private Specification<Employee> conjunct(List<Specification<Employee>> specifications) {
        Iterator<Specification<Employee>> it = specifications.iterator();
        Specification<Employee> specification = it.next();
        while (it.hasNext()) {
            specification = Specification.where(specification).and(it.next());
        }
        return specification;
    }
}
