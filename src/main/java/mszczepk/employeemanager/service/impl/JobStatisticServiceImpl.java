package mszczepk.employeemanager.service.impl;

import mszczepk.employeemanager.domain.dto.JobStatistic;
import mszczepk.employeemanager.domain.entity.Job;
import mszczepk.employeemanager.persistence.repository.EmployeeRepository;
import mszczepk.employeemanager.persistence.repository.JobRepository;
import mszczepk.employeemanager.service.JobStatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class JobStatisticServiceImpl implements JobStatisticService {
    private JobRepository jobRepository;
    private EmployeeRepository employeeRepository;

    @Autowired
    public JobStatisticServiceImpl(JobRepository jobRepository, EmployeeRepository employeeRepository) {
        this.jobRepository = jobRepository;
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<JobStatistic> getAllJobStatistics() {
        Stream<Job> jobs = jobRepository.findAll().stream();
        Stream<JobStatistic> jobStats = jobs.map(job -> {
            long employeesCount = employeeRepository.countByJob(job);
            return new JobStatistic(job, employeesCount);
        });
        return jobStats.collect(Collectors.toList());
    }
}
