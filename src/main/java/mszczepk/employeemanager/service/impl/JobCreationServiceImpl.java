package mszczepk.employeemanager.service.impl;

import mszczepk.employeemanager.domain.dto.JobCreationData;
import mszczepk.employeemanager.domain.dto.JobStatistic;
import mszczepk.employeemanager.domain.error.BadRequestException;
import mszczepk.employeemanager.domain.error.ConflictException;
import mszczepk.employeemanager.domain.error.StatusException;
import mszczepk.employeemanager.domain.entity.Job;
import mszczepk.employeemanager.persistence.repository.JobRepository;
import mszczepk.employeemanager.service.JobCreationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.Optional;

@Service
public class JobCreationServiceImpl implements JobCreationService {
    private JobRepository jobRepository;

    @Autowired
    public JobCreationServiceImpl(JobRepository jobRepository) {
        this.jobRepository = jobRepository;
    }

    @Override
    public JobStatistic createJob(JobCreationData jobRequest) throws StatusException {
        validate(jobRequest);
        String title = jobRequest.getTitle();
        Optional<Job> searchResult = jobRepository.findByTitle(title);
        if (searchResult.isPresent()) {
            throw new ConflictException();
        }
        Job job = createNewJob(title);
        job = jobRepository.saveAndFlush(job);
        return new JobStatistic(job);
    }

    private void validate(JobCreationData jobRequest) throws BadRequestException {
        String title = jobRequest.getTitle();
        if (title == null) {
            throw new BadRequestException("Job title must be specified.");
        } if (title.length() < 3) {
            throw new BadRequestException("Job title must contain at least 3 characters.");
        }
    }

    private Job createNewJob(String title) {
        Job job = new Job();
        job.setId(null); //null allows auto-generation
        job.setTitle(title);
        job.setCreatedOn(new Date());
        return job;
    }
}
