package mszczepk.employeemanager.service.impl;

import mszczepk.employeemanager.domain.dto.EmployeeInfo;
import mszczepk.employeemanager.domain.error.NotFoundException;
import mszczepk.employeemanager.domain.entity.Employee;
import mszczepk.employeemanager.persistence.repository.EmployeeRepository;
import mszczepk.employeemanager.service.EmployeeDeletionService;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class EmployeeDeletionServiceImpl implements EmployeeDeletionService {
    private EmployeeRepository employeeRepository;

    public EmployeeDeletionServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public EmployeeInfo deleteEmployee(Long id) throws NotFoundException {
        Optional<Employee> employee = employeeRepository.findById(id);
        if (employee.isPresent()) {
            employeeRepository.deleteById(id);
            return new EmployeeInfo(employee.get());
        } else {
            throw new NotFoundException();
        }
    }
}
