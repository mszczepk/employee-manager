package mszczepk.employeemanager.service.impl;

import mszczepk.employeemanager.domain.dto.EmployeeCreationData;
import mszczepk.employeemanager.domain.dto.EmployeeInfo;
import mszczepk.employeemanager.domain.error.BadRequestException;
import mszczepk.employeemanager.domain.error.ConflictException;
import mszczepk.employeemanager.domain.error.StatusException;
import mszczepk.employeemanager.domain.entity.Employee;
import mszczepk.employeemanager.domain.entity.Job;
import mszczepk.employeemanager.persistence.repository.EmployeeRepository;
import mszczepk.employeemanager.persistence.repository.JobRepository;
import mszczepk.employeemanager.service.EmployeeCreationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class EmployeeCreationServiceImpl implements EmployeeCreationService {
    private JobRepository jobRepository;
    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeCreationServiceImpl(JobRepository jobRepository, EmployeeRepository employeeRepository) {
        this.jobRepository = jobRepository;
        this.employeeRepository = employeeRepository;
    }

    @Override
    public EmployeeInfo createEmployee(EmployeeCreationData employeeRequest) throws StatusException {
        validate(employeeRequest);
        Job job = jobRepository.findByTitle(employeeRequest.getJob()).get();
        Employee employee = createNewEmployee(job, employeeRequest);
        employee = employeeRepository.saveAndFlush(employee);
        return new EmployeeInfo(employee);
    }

    private void validate(EmployeeCreationData employeeRequest) throws StatusException {
        List<String> problems = new ArrayList<>();
        String firstName = employeeRequest.getFirstName();
        if (firstName == null) {
            problems.add("First name must be specified.");
        } else if (firstName.isEmpty()) {
            problems.add("First name must contain at least 1 character.");
        }
        String lastName = employeeRequest.getLastName();
        if (lastName == null) {
            problems.add("Last name must be specified.");
        } else if (lastName.isEmpty()) {
            problems.add("Last name must contain at least 1 character.");
        }
        String email = employeeRequest.getEmail();
        if (email == null) {
            problems.add("E-mail must be specified.");
        } else if (email.length() < 5) {
            problems.add("E-mail must contain at least 5 characters.");
        } else if (!email.contains("@")) {
            problems.add("Invalid e-mail.");
        } else if (employeeRepository.existsByEmail(email)) {
            throw new ConflictException("E-mail already exists.");
        }
        String job = employeeRequest.getJob();
        if (job == null) {
            problems.add("Job must be specified.");
        } else if (!jobRepository.existsByTitle(job)) {
            problems.add("No such job.");
        }
        if (!problems.isEmpty()) {
            throw new BadRequestException(problems);
        }
    }

    private Employee createNewEmployee(Job job, EmployeeCreationData employeeRequest) {
        Employee employee = new Employee();
        employee.setId(null);
        employee.setFirstName(employeeRequest.getFirstName());
        employee.setLastName(employeeRequest.getLastName());
        employee.setEmail(employeeRequest.getEmail());
        employee.setJob(job);
        employee.setCreatedOn(new Date());
        return employee;
    }
}
