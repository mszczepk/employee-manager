package mszczepk.employeemanager.service;

import mszczepk.employeemanager.domain.dto.JobStatistic;
import java.util.List;

public interface JobStatisticService {
    List<JobStatistic> getAllJobStatistics();
}
