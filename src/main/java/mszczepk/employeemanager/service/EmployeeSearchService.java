package mszczepk.employeemanager.service;

import mszczepk.employeemanager.domain.dto.EmployeeInfo;
import mszczepk.employeemanager.domain.error.BadRequestException;
import java.util.List;
import java.util.Map;

public interface EmployeeSearchService {
    List<EmployeeInfo> getAll();

    List<EmployeeInfo> getAllBySpecification(Map<String, String> parameters) throws BadRequestException;
}
