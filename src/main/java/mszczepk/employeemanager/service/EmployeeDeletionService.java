package mszczepk.employeemanager.service;

import mszczepk.employeemanager.domain.dto.EmployeeInfo;
import mszczepk.employeemanager.domain.error.NotFoundException;

public interface EmployeeDeletionService {
    EmployeeInfo deleteEmployee(Long id) throws NotFoundException;
}
