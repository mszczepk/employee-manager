package mszczepk.employeemanager.persistence.repository;

import mszczepk.employeemanager.domain.entity.Employee;
import mszczepk.employeemanager.domain.entity.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface EmployeeRepository extends JpaRepository<Employee, Long>, JpaSpecificationExecutor<Employee> {
    long countByJob(Job job);

    long countByJobTitle(String jobTitle);

    boolean existsByEmail(String email);
}
