package mszczepk.employeemanager.persistence.query;

import mszczepk.employeemanager.domain.entity.Employee;
import org.springframework.data.jpa.domain.Specification;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class EmployeeSpecification implements Specification<Employee> {
    private String fieldName;
    private String fieldValue;

    public EmployeeSpecification(String fieldName, String fieldValue) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    @Override
    public Predicate toPredicate(Root<Employee> employeeClass,              //Root represents <Employee> model, something like table from database
                                 CriteriaQuery<?> query,                    //never seen this used in implementations?
                                 CriteriaBuilder criteriaBuilder) {         //Builder is used to relate a 'thing' to be searched with a value to be searched
        Path<?> field = employeeClass.get(fieldName);                       //Path is something like column from table
        return criteriaBuilder.equal(field, fieldValue);                    //we will be searching row which column is equal to the value
    }                                                                       //returned predicate is a fragment of query that separately define some information about what we search
}
