package mszczepk.employeemanager.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application.properties")
public class ApplicationStartupIntegrationTest {

    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate client;

    @Test
    public void getAnyResponseFromApplication() {
        //given
        String validRequestUrl = "http://localhost:" + port + "/";
        //when
        Object response = sendGetRequestTo(validRequestUrl);
        //then
        assertThat(response).isNotNull();
    }

    private Object sendGetRequestTo(String url) {
        return client.getForObject(url, Object.class);
    }

    @Test
    public void getNoResponseFromInvalidUrl() {
        //given
        int otherPort = port + 1;
        String invalidRequestUrl = "http://localhost:" + otherPort + "/";
        //when
        Throwable thrown = catchThrowable(() -> sendGetRequestTo(invalidRequestUrl));
        //then
        assertThat(thrown).isInstanceOf(RestClientException.class);
    }
}
