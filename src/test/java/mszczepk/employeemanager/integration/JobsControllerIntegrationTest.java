package mszczepk.employeemanager.integration;

import mszczepk.employeemanager.domain.entity.Employee;
import mszczepk.employeemanager.domain.entity.Job;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest //creates full context
@Transactional //enables usage of EntityManager and persist
@DirtiesContext(classMode = AFTER_EACH_TEST_METHOD) //ensures database cleanup after each test
@TestPropertySource("classpath:test.application.properties") //configures context exactly as defined in properties
public class JobsControllerIntegrationTest {
    @Autowired
    private WebApplicationContext context;
    @Autowired
    EntityManager database;
    private String url;
    private MockMvc mock;

    @Before
    public void setup() {
        url = "/jobs";
        mock = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void postValidDataShouldCreateJob() throws Exception {
        //given
        String requestBody = "{\"title\":\"Hobbit\"}";
        //when
        ResultActions result = mock.perform(post(url)
                .contentType(APPLICATION_JSON)
                .content(requestBody));
        //then
        result.andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.title", is("Hobbit")))
                .andExpect(jsonPath("$.employeesCount", is(0)));
    }

    @Test
    public void postSameDataTwiceShouldFail() throws Exception {
        //given
        String requestBody = "{\"title\":\"Hobbit\"}";
        mock.perform(post(url)
                .contentType(APPLICATION_JSON)
                .content(requestBody));
        //when
        ResultActions result = mock.perform(post(url)
                .contentType(APPLICATION_JSON)
                .content(requestBody));
        //then
        result.andExpect(status().isConflict());
    }

    @Test
    public void postDifferentDataTwiceShouldCreateTwoJobs() throws Exception {
        //given
        String firstRequestBody = "{\"title\":\"Hobbit\"}";
        String secondRequestBody = "{\"title\":\"Wizard\"}";
        //when
        ResultActions firstResult = mock.perform(post(url)
                .contentType(APPLICATION_JSON)
                .content(firstRequestBody));
        ResultActions secondResult = mock.perform(post(url)
                .contentType(APPLICATION_JSON)
                .content(secondRequestBody));
        //then
        firstResult.andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.title", is("Hobbit")))
                .andExpect(jsonPath("$.employeesCount", is(0)));
        secondResult.andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.title", is("Wizard")))
                .andExpect(jsonPath("$.employeesCount", is(0)));
    }

    @Test
    public void notCreateJobWhenNoData() throws Exception {
        //when
        ResultActions result = mock.perform(post(url).contentType(APPLICATION_JSON));
        //then
        result.andExpect(status().isBadRequest());
    }

    @Test
    public void notCreateJobWhenInvalidData() throws Exception {
        //given
        String emptyTitle = "{\"title\":\"\"}";
        String invalidField = "{\"abc\":\"ABC\"}";
        String[] requestBodies = {emptyTitle, invalidField};

        for (String invalidRequestBody : requestBodies) {
            //when
            ResultActions result = mock.perform(post(url)
                    .contentType(APPLICATION_JSON)
                    .content(invalidRequestBody));
            //then
            result.andExpect(status().isBadRequest());
        }
    }

    @Test //learning test
    public void acceptOnlyPostAndGet() throws Exception {
        //given
        List<ResultActions> results = new ArrayList<>();
        List<MockHttpServletRequestBuilder> notAllowedRequests = new ArrayList<>();
        notAllowedRequests.add(put(url));
        notAllowedRequests.add(patch(url));
        notAllowedRequests.add(delete(url));
        //when
        for (MockHttpServletRequestBuilder request : notAllowedRequests) {
            results.add(mock.perform(request));
        }
        //then
        for (ResultActions result : results) {
            result.andExpect(status().isMethodNotAllowed());
        }
    }

    @Test
    public void getShouldReturnAllJobs() throws Exception {
        //given
        jobsExistInDatabase("Wizard", "Hobbit", "Elf");
        //when
        ResultActions result = mock.perform(get(url).accept(APPLICATION_JSON));
        //then
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$[*].title", containsInAnyOrder("Wizard", "Hobbit", "Elf")));
    }

    private void jobsExistInDatabase(String... titles) {
        for (String title : titles) {
            Job job = new Job();
            job.setId(null);
            job.setTitle(title);
            job.setCreatedOn(new Date());
            database.persist(job);
            database.flush();
        }
    }

    @Test
    public void getShouldReturnEmployeesCount() throws Exception {
        //given
        jobsAndEmployeesExistInDatabase();
        //when
        ResultActions result = mock.perform(get(url).accept(APPLICATION_JSON));
        //then
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", hasSize(3)))
                .andExpect(jsonPath("$[?(@.title=='Wizard')].employeesCount", contains(2)))
                .andExpect(jsonPath("$[?(@.title=='Hobbit')].employeesCount", contains(1)))
                .andExpect(jsonPath("$[?(@.title=='Elf')].employeesCount", contains(0)));
    }

    private void jobsAndEmployeesExistInDatabase() {
        Job job1 = new Job();
        job1.setId(null);
        job1.setTitle("Wizard");
        job1.setCreatedOn(new Date());
        database.persist(job1);
        Job job2 = new Job();
        job2.setId(null);
        job2.setTitle("Hobbit");
        job2.setCreatedOn(new Date());
        database.persist(job2);
        Job job3 = new Job();
        job3.setId(null);
        job3.setTitle("Elf");
        job3.setCreatedOn(new Date());
        database.persist(job3);
        Employee employee1 = new Employee();
        employee1.setId(null);
        employee1.setFirstName("Gandalf");
        employee1.setLastName("The Grey");
        employee1.setEmail("gandalf.grey@lotr.com");
        employee1.setJob(job1);
        employee1.setCreatedOn(new Date());
        database.persist(employee1);
        Employee employee2 = new Employee();
        employee2.setId(null);
        employee2.setFirstName("Saruman");
        employee2.setLastName("The White");
        employee2.setEmail("saruman.white@lotr.com");
        employee2.setJob(job1);
        employee2.setCreatedOn(new Date());
        database.persist(employee2);
        Employee employee3 = new Employee();
        employee3.setId(null);
        employee3.setFirstName("Frodo");
        employee3.setLastName("Baggins");
        employee3.setEmail("frodo.baggins@lotr.com");
        employee3.setJob(job2);
        employee3.setCreatedOn(new Date());
        database.persist(employee3);
        database.flush();
    }
}
