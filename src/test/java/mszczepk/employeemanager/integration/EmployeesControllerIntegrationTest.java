package mszczepk.employeemanager.integration;

import mszczepk.employeemanager.domain.entity.Employee;
import mszczepk.employeemanager.domain.entity.Job;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import javax.persistence.EntityManager;
import java.util.Date;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@DirtiesContext(classMode = AFTER_EACH_TEST_METHOD)
@TestPropertySource("classpath:test.application.properties")
public class EmployeesControllerIntegrationTest {
    @Autowired
    private WebApplicationContext context;
    @Autowired
    EntityManager database;
    private String url;
    private MockMvc mock;

    @Before
    public void setup() {
        url = "/employees";
        mock = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void postValidDataShouldCreateEmployee() throws Exception {
        //given
        jobExistInDatabase();
        String requestBody = "{" +
                "\"firstName\":\"Gandalf\"," +
                "\"lastName\":\"The Grey\"," +
                "\"email\":\"gandalf.grey@lotr.com\"," +
                "\"job\":\"Wizard\"" +
                "}";
        //when
        ResultActions result = mock.perform(post(url)
                .contentType(APPLICATION_JSON)
                .content(requestBody));
        //then
        result.andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.firstName", is("Gandalf")))
                .andExpect(jsonPath("$.lastName", is("The Grey")))
                .andExpect(jsonPath("$.email", is("gandalf.grey@lotr.com")))
                .andExpect(jsonPath("$.job", is("Wizard")));
    }

    private void jobExistInDatabase() {
        Job job = new Job();
        job.setId(null);
        job.setTitle("Wizard");
        job.setCreatedOn(new Date());
        database.persist(job);
        database.persist(job);
    }

    @Test
    public void postSameDataTwiceShouldFail() throws Exception {
        //given
        jobExistInDatabase();
        String requestBody = "{" +
                "\"firstName\":\"Gandalf\"," +
                "\"lastName\":\"The Grey\"," +
                "\"email\":\"gandalf.grey@lotr.com\"," +
                "\"job\":\"Wizard\"" +
                "}";
        mock.perform(post(url)
                .contentType(APPLICATION_JSON)
                .content(requestBody));
        //when
        ResultActions result = mock.perform(post(url)
                .contentType(APPLICATION_JSON)
                .content(requestBody));
        //then
        result.andExpect(status().isConflict());
    }

    @Test
    public void postDifferentDataTwiceShouldCreateTwoEmployee() throws Exception {
        //given
        jobExistInDatabase();
        String firstRequestBody = "{" +
                "\"firstName\":\"Gandalf\"," +
                "\"lastName\":\"The Grey\"," +
                "\"email\":\"gandalf.grey@lotr.com\"," +
                "\"job\":\"Wizard\"" +
                "}";
        String secondRequestBody = "{" +
                "\"firstName\":\"Saruman\"," +
                "\"lastName\":\"The White\"," +
                "\"email\":\"saruman.white@lotr.com\"," +
                "\"job\":\"Wizard\"" +
                "}";
        //when
        ResultActions firstResult = mock.perform(post(url)
                .contentType(APPLICATION_JSON)
                .content(firstRequestBody));
        ResultActions secondResult = mock.perform(post(url)
                .contentType(APPLICATION_JSON)
                .content(secondRequestBody));
        //then
        firstResult.andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.firstName", is("Gandalf")))
                .andExpect(jsonPath("$.lastName", is("The Grey")))
                .andExpect(jsonPath("$.email", is("gandalf.grey@lotr.com")))
                .andExpect(jsonPath("$.job", is("Wizard")));
        secondResult.andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(3)))
                .andExpect(jsonPath("$.firstName", is("Saruman")))
                .andExpect(jsonPath("$.lastName", is("The White")))
                .andExpect(jsonPath("$.email", is("saruman.white@lotr.com")))
                .andExpect(jsonPath("$.job", is("Wizard")));
    }

    @Test
    public void notCreateEmployeeWhenNoData() throws Exception {
        //when
        ResultActions result = mock.perform(post(url).contentType(APPLICATION_JSON));
        //then
        result.andExpect(status().isBadRequest());
    }

    @Test
    public void notCreateEmployeeWhenJobNotExist() throws Exception {
        //given
        String requestBody = "{" +
                "\"firstName\":\"Gandalf\"," +
                "\"lastName\":\"The Grey\"," +
                "\"email\":\"gandalf.grey@lotr.com\"," +
                "\"job\":\"Wizard\"" +
                "}";
        //when
        ResultActions result = mock.perform(post(url)
                .contentType(APPLICATION_JSON)
                .content(requestBody));
        //then
        result.andExpect(status().isBadRequest());
    }

    @Test
    public void notCreateEmployeeWhenInvalidData() throws Exception {
        //given
        String emptyFields = "{" +
                "\"firstName\":\"Gandalf\"," +
                "\"lastName\":\"\"," +
                "\"email\":\"\"," +
                "\"job\":\"\"" +
                "}";
        String missingFields = "{\"firstName\":\"Gandalf\"}";
        String nullFields = "{" +
                "\"firstName\":\"Gandalf\"," +
                "\"lastName\":null," +
                "\"email\":null," +
                "\"job\":null" +
                "}";
        String invalidField = "{\"abc\":\"ABC\"}";
        String[] requestBodies = {emptyFields, missingFields, nullFields, invalidField};

        for (String invalidRequestBody : requestBodies) {
            //when
            ResultActions result = mock.perform(post(url)
                    .contentType(APPLICATION_JSON)
                    .content(invalidRequestBody));
            //then
            result.andExpect(status().isBadRequest());
        }
    }

    @Test
    public void getShouldReturnAllEmployees() throws Exception {
        //given
        employeesExistInDatabase();
        //when
        ResultActions result = mock.perform(get(url).accept(APPLICATION_JSON));
        //then
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$[*].id", containsInAnyOrder(2, 3)))
                .andExpect(jsonPath("$[?(@.id==2)].firstName", contains("Gandalf")))
                .andExpect(jsonPath("$[?(@.id==2)].lastName", contains("The Grey")))
                .andExpect(jsonPath("$[?(@.id==2)].email", contains("gandalf.grey@lotr.com")))
                .andExpect(jsonPath("$[?(@.id==2)].job", contains("Wizard")))
                .andExpect(jsonPath("$[?(@.id==3)].firstName", contains("Saruman")))
                .andExpect(jsonPath("$[?(@.id==3)].lastName", contains("The White")))
                .andExpect(jsonPath("$[?(@.id==3)].email", contains("saruman.white@lotr.com")))
                .andExpect(jsonPath("$[?(@.id==3)].job", contains("Wizard")));
    }

    private void employeesExistInDatabase() {
        Job job1 = new Job();
        job1.setId(null);
        job1.setTitle("Wizard");
        job1.setCreatedOn(new Date());
        database.persist(job1);
        Employee employee1 = new Employee();
        employee1.setId(null);
        employee1.setFirstName("Gandalf");
        employee1.setLastName("The Grey");
        employee1.setEmail("gandalf.grey@lotr.com");
        employee1.setJob(job1);
        employee1.setCreatedOn(new Date());
        database.persist(employee1);
        Employee employee2 = new Employee();
        employee2.setId(null);
        employee2.setFirstName("Saruman");
        employee2.setLastName("The White");
        employee2.setEmail("saruman.white@lotr.com");
        employee2.setJob(job1);
        employee2.setCreatedOn(new Date());
        database.persist(employee2);
        database.flush();
    }

    @Test
    public void deleteShouldReturnDeletedEmployee() throws Exception {
        //given
        employeesExistInDatabase();
        //when
        ResultActions result = mock.perform(delete(url + "/2"));
        //then
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.firstName", is("Gandalf")))
                .andExpect(jsonPath("$.lastName", is("The Grey")))
                .andExpect(jsonPath("$.email", is("gandalf.grey@lotr.com")))
                .andExpect(jsonPath("$.job", is("Wizard")));
    }

    @Test
    public void deleteInvalidIdShouldNotFound() throws Exception {
        //when
        ResultActions result = mock.perform(delete(url + "/200"));
        //then
        result.andExpect(status().isNotFound());
    }

    @Test
    public void getAfterDeleteShouldEmployeeBeRemoved() throws Exception {
        //given
        employeesExistInDatabase();
        mock.perform(delete(url + "/3"));
        //when
        ResultActions result = mock.perform(get(url).accept(APPLICATION_JSON));
        //then
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$[*].id", containsInAnyOrder(2)))
                .andExpect(jsonPath("$[?(@.id==2)].firstName", contains("Gandalf")))
                .andExpect(jsonPath("$[?(@.id==2)].lastName", contains("The Grey")))
                .andExpect(jsonPath("$[?(@.id==2)].email", contains("gandalf.grey@lotr.com")))
                .andExpect(jsonPath("$[?(@.id==2)].job", contains("Wizard")));
    }

    @Test
    public void getWithSearchShouldReturnEmployees() throws Exception {
        //given
        employeesExistInDatabase();
        String searchParams = "?firstName=Gandalf";
        //when
        ResultActions result = mock.perform(get(url + searchParams).accept(APPLICATION_JSON));
        //then
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$[*].id", containsInAnyOrder(2)))
                .andExpect(jsonPath("$[?(@.id==2)].firstName", contains("Gandalf")))
                .andExpect(jsonPath("$[?(@.id==2)].lastName", contains("The Grey")))
                .andExpect(jsonPath("$[?(@.id==2)].email", contains("gandalf.grey@lotr.com")))
                .andExpect(jsonPath("$[?(@.id==2)].job", contains("Wizard")));
    }

    @Test
    public void invalidSearchValuesShouldReturnEmptyList() throws Exception {
        //given
        employeesExistInDatabase();
        String noValue = "?firstName"; //results value to be null
        String invalidName = "?firstName=Frodo";
        String[] searchParams = {noValue, invalidName};

        for (String search : searchParams) {
            //when
            ResultActions result = mock.perform(get(url + search).accept(APPLICATION_JSON));
            //then
            result.andExpect(status().isOk())
                    .andExpect(jsonPath("$[*]", hasSize(0)));
        }
    }

    @Test
    public void invalidSearchParamsShouldFail() throws Exception {
        //given
        String invalidParam = "?invalid=Frodo";
        String invalidParams = "?232=23&dfs=232&";
        String invalidParams2 = "?abcdefgh";
        String invalidParams3 = "?12=&&&=??xd";
        String[] ivalidSearchParams = {invalidParam, invalidParams, invalidParams2, invalidParams3};

        for (String search : ivalidSearchParams) {
            //when
            ResultActions result = mock.perform(get(url + search).accept(APPLICATION_JSON));
            //then
            result.andExpect(status().isBadRequest());
        }
    }
}
