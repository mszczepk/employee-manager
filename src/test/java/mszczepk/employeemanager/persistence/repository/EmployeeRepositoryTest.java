package mszczepk.employeemanager.persistence.repository;

import mszczepk.employeemanager.domain.entity.Employee;
import mszczepk.employeemanager.domain.entity.Job;
import mszczepk.employeemanager.persistence.query.EmployeeSpecification;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Date;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD;

@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = AFTER_EACH_TEST_METHOD)
@AutoConfigureTestDatabase(replace = NONE)
@TestPropertySource("classpath:test.application.properties")
public class EmployeeRepositoryTest {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private JobRepository jobRepository;

    @Test
    public void givenExactJobShouldCountEmployees() {
        //given
        Job job = jobExistingInDatabase();
        employeesHavingJobExistInDatabase(job);
        //when
        Long employeesCount = employeeRepository.countByJob(job);
        //then
        assertThat(employeesCount).isEqualTo(2L);
    }

    private Job jobExistingInDatabase() {
        Job job = new Job();
        job.setId(null);
        job.setTitle("Wizard");
        job.setCreatedOn(new Date());
        jobRepository.saveAndFlush(job);
        return job;
    }

    private void employeesHavingJobExistInDatabase(Job job) {
        Employee employee1 = new Employee();
        employee1.setId(null);
        employee1.setFirstName("Gandalf");
        employee1.setLastName("The Grey");
        employee1.setEmail("gandalf.grey@lotr.com");
        employee1.setJob(job);
        employee1.setCreatedOn(new Date());
        employeeRepository.saveAndFlush(employee1);

        Employee employee2 = new Employee();
        employee2.setId(null);
        employee2.setFirstName("Saruman");
        employee2.setLastName("The White");
        employee2.setEmail("saruman.white@lotr.com");
        employee2.setJob(job);
        employee2.setCreatedOn(new Date());
        employeeRepository.saveAndFlush(employee2);
    }

    @Test @Ignore
    public void givenQueryJobShouldCountEmployees() { //fails because query job is not managed object?
        //given
        jobAndEmployeesExistInDatabase();
        Job job = jobWithOnlyQueryData();
        //when
        Long employeesCount = employeeRepository.countByJob(job);
        //then
        assertThat(employeesCount).isEqualTo(2L);
    }

    private void jobAndEmployeesExistInDatabase() {
        Job job = new Job();
        job.setId(null);
        job.setTitle("Wizard");
        job.setCreatedOn(new Date());
        jobRepository.saveAndFlush(job);

        Employee employee1 = new Employee();
        employee1.setId(null);
        employee1.setFirstName("Gandalf");
        employee1.setLastName("The Grey");
        employee1.setEmail("gandalf.grey@lotr.com");
        employee1.setJob(job);
        employee1.setCreatedOn(new Date());
        employeeRepository.saveAndFlush(employee1);

        Employee employee2 = new Employee();
        employee2.setId(null);
        employee2.setFirstName("Saruman");
        employee2.setLastName("The White");
        employee2.setEmail("saruman.white@lotr.com");
        employee2.setJob(job);
        employee2.setCreatedOn(new Date());
        employeeRepository.saveAndFlush(employee2);
    }

    private Job jobWithOnlyQueryData() {
        Job job = new Job();
        job.setId(null);
        job.setTitle("Wizard");
        job.setCreatedOn(null);
        return job;
    }

    @Test
    public void countEmployeesByJobTitle() {
        //given
        jobAndEmployeesExistInDatabase();
        //when
        Long employeesCount = employeeRepository.countByJobTitle("Wizard");
        //then
        assertThat(employeesCount).isEqualTo(2L);
    }

    @Test
    public void countEmployeesByJobInvalidTitle() {
        //given empty database
        //when
        Long employeesCount = employeeRepository.countByJobTitle("Wizard");
        //then
        assertThat(employeesCount).isEqualTo(0L);
    }

    @Test
    public void notPersistEmployeeWhenJobNotExist() { //fails
        //given
        Job job = new Job();
        job.setId(null);
        job.setTitle("Hobbit");
        job.setCreatedOn(new Date());
        Employee employee = new Employee();
        employee.setId(null);
        employee.setFirstName("Frodo");
        employee.setLastName("Baggins");
        employee.setEmail("frodo.baggins@lotr.com");
        employee.setJob(job);
        employee.setCreatedOn(new Date());
        //when
        Throwable thrown = catchThrowable(() -> employeeRepository.saveAndFlush(employee));
        //then
        assertThat(thrown).isNotNull();
    }

    @Test
    public void notPersistEmployeesWithSameEmail() {
        //given
        Job job = jobAndEmployeeExistInDatabase();
        Employee employee = new Employee();
        employee.setId(null);
        employee.setFirstName("Bilbo");
        employee.setLastName("Bagosz");
        employee.setEmail("frodo.baggins@lotr.com");
        employee.setJob(job);
        employee.setCreatedOn(new Date());
        //when
        Throwable thrown = catchThrowable(() -> employeeRepository.saveAndFlush(employee));
        //then
        assertThat(thrown).isNotNull();
    }

    private Job jobAndEmployeeExistInDatabase() {
        Job job = new Job();
        job.setId(null);
        job.setTitle("Hobbit");
        job.setCreatedOn(new Date());
        jobRepository.saveAndFlush(job);

        Employee employee = new Employee();
        employee.setId(null);
        employee.setFirstName("Frodo");
        employee.setLastName("Baggins");
        employee.setEmail("frodo.baggins@lotr.com");
        employee.setJob(job);
        employee.setCreatedOn(new Date());
        employeeRepository.saveAndFlush(employee);
        return job;
    }

    @Test
    public void deleteInvalidIdShouldFail() {
        //when
        Throwable thrown = catchThrowable(() -> employeeRepository.deleteById(100L));
        //then
        assertThat(thrown).isNotNull();
    }

    @Test
    public void findByFirstNameSpecification() {
        //given
        jobAndEmployeesExistInDatabase();
        EmployeeSpecification specification = new EmployeeSpecification("firstName", "Gandalf");
        //when
        List<Employee> result = employeeRepository.findAll(specification);
        //then
        assertThat(result).hasSize(1);
        Employee employee = result.get(0);
        assertThat(employee.getId()).isEqualTo(2L);
        assertThat(employee.getFirstName()).isEqualTo("Gandalf");
        assertThat(employee.getLastName()).isEqualTo("The Grey");
        assertThat(employee.getEmail()).isEqualTo("gandalf.grey@lotr.com");
        assertThat(employee.getJob().getTitle()).isEqualTo("Wizard");
        assertThat(employee.getCreatedOn()).isCloseTo(new Date(), 30000L);
    }

    @Test
    public void findByFirstNameAndLastNameSpecifications() {
        //given
        twoEmployeesWithSameNamesExistInDatabase();
        EmployeeSpecification hasFirstName = new EmployeeSpecification("firstName", "Frodo");
        EmployeeSpecification hasLastName = new EmployeeSpecification("lastName", "Baggins");
        Specification<Employee> hasFirstAndLastName = Specification.where(hasFirstName).and(hasLastName);
        //when
        List<Employee> result = employeeRepository.findAll(hasFirstAndLastName);
        //then
        assertThat(result).hasSize(2);
        Employee employee1 = result.get(0);
        assertThat(employee1.getId()).isEqualTo(2L);
        assertThat(employee1.getFirstName()).isEqualTo("Frodo");
        assertThat(employee1.getLastName()).isEqualTo("Baggins");
        assertThat(employee1.getEmail()).isEqualTo("frodo.baggins@lotr.com");
        assertThat(employee1.getJob().getTitle()).isEqualTo("Hobbit");
        assertThat(employee1.getCreatedOn()).isCloseTo(new Date(), 30000L);
        Employee employee2 = result.get(1);
        assertThat(employee2.getId()).isEqualTo(3L);
        assertThat(employee2.getFirstName()).isEqualTo("Frodo");
        assertThat(employee2.getLastName()).isEqualTo("Baggins");
        assertThat(employee2.getEmail()).isEqualTo("frodo.baggins2@lotr.com");
        assertThat(employee2.getJob().getTitle()).isEqualTo("Hobbit");
        assertThat(employee2.getCreatedOn()).isCloseTo(new Date(), 30000L);

    }

    private void twoEmployeesWithSameNamesExistInDatabase() {
        Job job = new Job();
        job.setId(null);
        job.setTitle("Hobbit");
        job.setCreatedOn(new Date());
        jobRepository.saveAndFlush(job);

        Employee employee1 = new Employee();
        employee1.setId(null);
        employee1.setFirstName("Frodo");
        employee1.setLastName("Baggins");
        employee1.setEmail("frodo.baggins@lotr.com");
        employee1.setJob(job);
        employee1.setCreatedOn(new Date());
        employeeRepository.saveAndFlush(employee1);

        Employee employee2 = new Employee();
        employee2.setId(null);
        employee2.setFirstName("Frodo");
        employee2.setLastName("Baggins");
        employee2.setEmail("frodo.baggins2@lotr.com");
        employee2.setJob(job);
        employee2.setCreatedOn(new Date());
        employeeRepository.saveAndFlush(employee2);

        Employee employee3 = new Employee();
        employee3.setId(null);
        employee3.setFirstName("Bilbo");
        employee3.setLastName("Baggins");
        employee3.setEmail("bilbo.baggins@lotr.com");
        employee3.setJob(job);
        employee3.setCreatedOn(new Date());
        employeeRepository.saveAndFlush(employee3);
    }
}
