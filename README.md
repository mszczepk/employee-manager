# Employee Manager
REST application for managing employee data



## Get started
1. Make sure you have Java Development Kit 8 or above installed on your machine.  
   A REST client may be useful too (eg. POSTMAN, available at
   [the link](https://www.getpostman.com/apps)).
2. Get copy of the project repository. You can use the command below if you have Git.  
   `git clone git@gitlab.com:mszczepk/employee-manager.git`
3. To startup application, enter terminal in the project directory and use commands:  
   `gradlew bootJar`  
   `java -jar build/libs/employee-manager-1.0.0.jar`
4. The application is available at `http://localhost:7654`. Resources are:  
   `/jobs` - GET and POST http methods  
   `/employees` - POST and GET with search parameters  
   `/employees/{id}` - DELETE method
5. To shutdown application, press `Ctrl`+`C` in the same terminal session.
6. To clear application data delete `data.mv.db` file or execute `gradlew clean`.



## Usage
Request URL starts with `http://localhost:7654`
* To create a new employee:
  1. send POST with job JSON in the body at `/jobs`
     ```
     {
        "title": "Wizard"
     }
     ```
  2. send POST with employee JSON in the body at `/employees`
     ```
     {
        "firstName": "Gandalf",
        "lastName": "The Grey",
        "email": "gandalf.grey@lotr.com",
        "job": "Wizard"
     }
     ```
* To get the list of all employees, send GET at `/employees`  
* To find employees by first name, last name or e-mail, append GET with search parameters, eg:  
  `http://localhost:7654/employees?firstName=Gandalf&lastName=The%20Grey`  
  `http://localhost:7654/employees?email=gandalf.grey@lotr.com`
* To get the list of jobs along with the number of employees assigned, send GET at `/jobs` 
* To remove an employee from the list, send DELETE at `/employees/{id}`, eg:  
  `http://localhost:7654/employees/2`



## Testing
Enter terminal in the project directory and use commands:  
`gradlew test` - to run unit tests  
`gradlew integrationTest` - to run integration tests



## Development
### Encoding
ISO-8859-1 - files with `.properties` extension  
UTF-8 - all other files

### Line separators
Specified in the `.gitattributes` file.  
**Remember to update the file with new extensions if needed!**

### TODO
* Converters - exceptions/entities -> DTO
* Validators - refactor DTO validation from services to separate validator classes
* Test data set - unify the data that is often repeated in tests, manage it in separate class
* HATEOAS - add resource URLs to all responses
* Unify exception handling for whole application
* Pagination
* Security
* PostgreSQL - instead of in memory H2 database



## Author
Mateusz Szczepkowski
